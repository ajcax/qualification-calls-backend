const fs = require('fs')
const sql = require('mssql')

const sslCert = fs.readFileSync('C:\\Users\\acandiales\\mykey.p12')

const config = {
  user: 'ENGAGE_IKE',
  password: 'SAENG.IKE.2017',
  server: '186.122.177.49',
  database: 'ENGAGE_IKE',
  options: {
    encrypt: true,
    trustServerCertificate: true
  },
  port: 1533,
  ssl: {
    rejectUnauthorized: true,
    ca: [sslCert],
    cert: [sslCert],
    key: null
  }
}

const pool = new sql.ConnectionPool(config)

pool.connect(err => {
  if (err) {
    console.log(err)
  } else {
    console.log('Conectado a la base de datos')
  }
})


module.exports = pool
module.exports.sql = sql;