const pool = require('./db')
const express = require('express')
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv').config()


const app = express()

app.use(cors());
app.use(bodyParser.json());

const sql = pool.sql

app.listen(8080, () => {
  console.log('Servidor escuchando en el puerto 8080')
})

app.post('/api/save-qualification-call', (req, res) => {

    const data = {
      phone:  req.body.phone,
      tags: req.body.tags,
      date: req.body.current_date
    }
    
    pool.request()
      .input('phone', sql.NVarChar, data.phone)
      .input('qualification_description', sql.NVarChar, data.tags)
      .input('date', sql.NVarChar, data.date)
      .query('INSERT INTO QUALIFICATION_CALLS (phone, qualification_description, date) VALUES (@phone, @qualification_description, @date)')
      .then(result => {
        res.status(200).json({
          status: 'success',
          message:'La calificación se ha guardado correctamente'
        })
      })
      .catch(error => {
        // console.log(error)
        res.status(500).json({
          status: 'error',
          message:'No se ha podido guardar la calificación'
        })
      })   
  })
  
  